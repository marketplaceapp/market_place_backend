import asyncMiddleware from '../../middleware/asyncMiddleware.js'

import { SuccessResponse, FailureResponse, } from '../../models/response/globalResponse.js';
import { error_messages, global_messages, success_messages } from '../../utils/constants.js';
import { isValidMongooseObjectId } from '../../utils/utils.js';

import { Product } from '../../models/products/index.js';
import { User } from '../../models/users/index.js';

export const getAllUser = asyncMiddleware(async (req, res) => {
    const user = await User.find()
    if(user) res.status(200).json(new SuccessResponse(success_messages.FETCH_USERS, user));
});


export const getUserById = asyncMiddleware(async (req, res) => {
    if (!isValidMongooseObjectId(req.params.id)) return res.status(400).json(new FailureResponse(global_messages.INVALID_PRODUCT_ID));
    
    const product = await Product.findById(req.params.id)
    //.populate('category');

    if (!product) return res.status(200).json(new SuccessResponse(error_messages.FETCH_PRODUCT_BY_ID));

    res.status(200).json(new SuccessResponse(success_messages.FETCH_PRODUCT_BY_ID, product));
});



export const addUser = asyncMiddleware(async (req, res) => {

    const file = req.file;
    const { name, description, price, discount_amount, category, sub_category, } = req.body

    // const category = await Category.findById(req.body.category);
    // if (!category) return res.status(400).json(new FailureResponse(error_messages.INVALID_CATEGORY_NAME));

    if (file?.length == 0) return res.status(400).json(new FailureResponse(global_messages.NO_FILE_UPLOADED));

    const basePath = `${req.protocol}://${req.get('host')}/public/uploads/`;
    const image_url = `${basePath}${file.filename}`

    let product = new Product({
        name, description, price, discount_amount, category, sub_category,
        image: image_url,
    });

    product = await product.save();


    if (!product) return res.status(200).json(new FailureResponse(error_messages.ADD_PRODUCT));

    res.status(200).json(new SuccessResponse(success_messages.ADD_PRODUCT, product));
});


export const deleteAllUser = asyncMiddleware(async (req, res) => {
    const result = await Product.deleteMany({});
    if (result?.deletedCount > 0) 
        return res.status(200).json(new SuccessResponse(success_messages.DELETE_ALL_PRODUCTS));
    else
        return res.status(200).json(new FailureResponse(error_messages.DELETE_ALL_PRODUCTS));
});