
import mongoose from 'mongoose';

import asyncMiddleware from '../../middleware/asyncMiddleware.js'


import { SuccessResponse, FailureResponse, } from '../../models/response/globalResponse.js';
import { error_messages, global_messages, success_messages } from '../../utils/constants.js';

import { Product } from '../../models/products/index.js';
import { isValidMongooseObjectId } from '../../utils/utils.js';


export const getAllProduct = asyncMiddleware(async (req, res) => {
    const product = await Product.find()
    
    if(product) res.status(200).json(new SuccessResponse(success_messages.FETCH_PRODUCTS, product));
});

export const getProductAds = asyncMiddleware(async (req, res) => {
    const product = await Product.find({ 
        "adsOwner._id": { $ne: req.user._id },
        status: 'active' 
    });

    if(product) res.status(200).json(new SuccessResponse(success_messages.FETCH_PRODUCTS, product));
});

export const getOwnersProductAds = asyncMiddleware(async (req, res) => {
    const product = await Product.find({ "adsOwner._id": req.user._id });

    if(product) res.status(200).json(new SuccessResponse(success_messages.FETCH_PRODUCTS, product));
});



export const getProductById = asyncMiddleware(async (req, res) => {
    if (!isValidMongooseObjectId(req.params.id)) return res.status(400).json(new FailureResponse(global_messages.INVALID_PRODUCT_ID));
    
    const product = await Product.findById(req.params.id)
    //.populate('category');

    if (!product) return res.status(200).json(new SuccessResponse(error_messages.FETCH_PRODUCT_BY_ID));

    res.status(200).json(new SuccessResponse(success_messages.FETCH_PRODUCT_BY_ID, product));
});



export const addProduct = asyncMiddleware(async (req, res) => {

    const file = req.file;
    const { name, description, price, discount_amount, category, sub_category, } = req.body

    // const category = await Category.findById(req.body.category);
    // if (!category) return res.status(400).json(new FailureResponse(error_messages.INVALID_CATEGORY_NAME));

    if (file?.length == 0) return res.status(400).json(new FailureResponse(global_messages.NO_FILE_UPLOADED));

    const basePath = `${req.protocol}://${req.get('host')}/public/uploads/`;
    const image_url = `${basePath}${file.filename}`

    let product = new Product({
        name, description, price, discount_amount, category, sub_category,
        image: image_url,
        adsOwner: req.user
    });

    product = await product.save();


    if (!product) return res.status(200).json(new FailureResponse(error_messages.ADD_PRODUCT));

    res.status(200).json(new SuccessResponse(success_messages.ADD_PRODUCT, product));
});

export const updateProduct = asyncMiddleware(async (req, res) => {
   
    if (!req.body.productIds.every(isValidMongooseObjectId)) {
        return res.status(400).json(new FailureResponse(global_messages.INVALID_PRODUCT_ID));
    }

    const productIds = req.body.productIds;

    const updatedProducts = await Product.updateMany(
        { _id: { $in: productIds } }, 
        { $set: { status: 'sold' } },
        { new: true }
    );

    if (updatedProducts.nModified === 0) {
        return res.status(200).json(new FailureResponse(error_messages.UPDATE_PRODUCT));
    }

    res.status(200).json(new SuccessResponse(success_messages.UPDATE_PRODUCT, updatedProducts));
});


export const deleteProduct = asyncMiddleware(async (req, res) => {
    if (!isValidMongooseObjectId(req.params.id)) return res.status(400).json(new FailureResponse(global_messages.INVALID_PRODUCT_ID));
    
    const product = await Product.findByIdAndDelete(req.params.id);

    if (!product) return res.status(200).json(new FailureResponse(error_messages.DELETE_PRODUCT));

    res.status(200).json(new SuccessResponse(success_messages.DELETE_PRODUCT, product));
});
    

export const deleteAllProduct = asyncMiddleware(async (req, res) => {
    const result = await Product.deleteMany({});
    if (result?.deletedCount > 0) 
        return res.status(200).json(new SuccessResponse(success_messages.DELETE_ALL_PRODUCTS));
    else
        return res.status(200).json(new FailureResponse(error_messages.DELETE_ALL_PRODUCTS));
});