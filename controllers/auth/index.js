
import bcrypt from 'bcryptjs'
import jwt from 'jsonwebtoken'

import asyncMiddleware from '../../middleware/asyncMiddleware.js'


import { SuccessResponse, FailureResponse, } from '../../models/response/globalResponse.js';
import { error_messages, global_messages, success_messages } from '../../utils/constants.js';
import { isValidMongooseObjectId } from '../../utils/utils.js';

import { User } from '../../models/users/index.js';


export const registerUser = asyncMiddleware(async (req, res) => {
    let user = new User({
        name: req.body.name,
        email: req.body.email,
        password: bcrypt.hashSync(req.body.password, 10),
        number: req.body.number,
        profile_url: req.body.profile_url,
    });

    user = await user.save();


    if (!user) return res.status(200).json(new FailureResponse(error_messages.ADD_USER));

    res.status(200).json(new SuccessResponse(success_messages.ADD_USER, user));
});



export const loginUser = asyncMiddleware(async (req, res) => {

    const user = await User.findOne({email: req.body.email})
    const secret = process.env.secret;
    if(!user) {
        return res.status(400).json(new FailureResponse(global_messages.INVALID_EMAIL, user));
    }

    if(user && bcrypt.compareSync(req.body.password, user.password)) {
        const token = jwt.sign(
            {
                userId: user.id,
            },
            secret,
            {expiresIn : '30d'}
        )
        
        res.status(200).json(new SuccessResponse(success_messages.USER_LOGIN, user, token ));
    } else {
        return res.status(400).json(new FailureResponse(global_messages.INVALID_PASSWORD, user));
    }
});

