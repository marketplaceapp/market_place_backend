import multer from "multer";
import path from 'path';

import { v4 as uuidv4 } from 'uuid';
import mongoose from 'mongoose';

import { daysFilterFunction, defaultMaxDays } from "../config/constants/globalConstants.js";

function validateTheStringInput(input) {
    return input && typeof input === 'string' && input.trim().length > 0;
}

function getPreviousDate(days) {
	const currentDate = new Date()
	const previousDate = new Date(currentDate.getTime() - days * 24 * 60 * 60 * 1000)
	return previousDate
}

// const storage = multer.diskStorage({
//     destination: function (req, file, cb) {
//         cb(null, 'public/uploads');
//     },
//     filename: function (req, file, cb) {
//         cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
//     }
// });

  
// export const uploadOptions = multer({ storage: storage });

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, "public/uploads/");
    },
    filename: function (req, file, cb) {
      const fileName = `${Date.now()}_${file.originalname}`;
      cb(null, fileName);
    },
});
  

export const uploadOptions = multer({ storage: storage });
  

export const getDays = (day) => {
    if (day) {
        const x = daysFilterFunction?.[day]?.() ?? 3650;
        return { $gte: getPreviousDate(x) };
    } else {
        return { $gte: getPreviousDate(defaultMaxDays) };
    }
};

export const getFullName = (user) => {
    return user?.fullName ?? (user?.email?.split('@')[0]) ?? '';
};


export const isValidDate = (date) => {
    try {
        if (validateTheStringInput(date) && Date.parse(date)) {
            return true;
        } else {
            return false;
        }
    } catch (error) {
        logger.error(error);
        return false;
    }
};


export const generateUUID = () => {
    const uuid = uuidv4();
   
    const id = uuid.substring(0, 8).replace(/-/g, '');
    return (id)?.toUpperCase();
}

export const isValidMongooseObjectId = (id) => {
    if (!mongoose.isValidObjectId(id)) 
        return false
    else
        return true
}
