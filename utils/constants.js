export const global_messages = {
    NO_TOKEN: 'Access denied. No token provided.',
    INVALID_TOKEN: 'Access denied. Invalid Token.',

    INVALID_MONGOOSE_ID: 'Invalid mongoose object id!',

    INVALID_ADMIN_ID: 'Invalid admin object id!',
    INVALID_USER_ID: 'Invalid user object id!',
    INVALID_INVOICE_ID: 'Invalid invoice object id',

    INVALID_FABRIC_ID: 'Invalid fabric object id!',
    INVALID_CUSTOMER_ID: 'Invalid customer object id!',
    INVALID_ORDER_BOOK_ID: 'Invalid order book object id!',
    INVALID_EMPLOYEE_ID: 'Invalid employee object id!',
    INVALID_PRODUCT_ID: 'Invalid product object id!',
    INVALID_CATEGORY_ID: 'Invalid category object id!',

    INVALID_PASSWORD: 'Invalid password!',
    INVALID_EMAIL: 'Invalid email!',

    MODEL_EXIST: 'Model already exist!',
    MODEL_NOT_EXIST: 'Model does not exist!',
    MODEL_DATA_DELETED: 'Data deleted from model',

    INTERNAL_SERVER_ERROR: 'Internal Server Error!',
    UNKNOWN_SHEET: 'Unknown sheet',
  

    MISSING_PARAMETERS: 'Required Parameters Missing!',
    NO_FILE_UPLOADED: 'No file uploaded!',
    
    SKIPPING_CATEGORY_RECORD_MISSING: 'Skipping category record due to missing parameters!',
    SKIPPING_PRODUCT_RECORD_MISSING: 'Skipping product record due to missing parameters!',
    SKIPPING_CUSTOMER_RECORD_MISSING: 'Skipping customer record due to missing parameters!',
    SKIPPING_FABRIC_RECORD_MISSING: 'Skipping fabric record due to missing parameters!',
    SKIPPING_EMPLOYEE_RECORD_MISSING: 'Skipping employee record due to missing parameters!',

    SKIPPING_CATEGORY_RECORD_EXIST: 'Skipping category record because category already exists!',
    SKIPPING_PRODUCT_RECORD_EXIST: 'Skipping product record because category already exists!',
    SKIPPING_CUSTOMER_RECORD_EXIST: 'Skipping customer record because customer already exists!',
    SKIPPING_FABRIC_RECORD_EXIST: 'Skipping fabric record because fabric already exists!',
    SKIPPING_EMPLOYEE_RECORD_EXIST: 'Skipping employee record because employee already exists!',

    FAILED_ADDING_CATEGORY_RECORD: 'Failed to add category record!',
    FAILED_ADDING_PRODUCT_RECORD: 'Failed to add product record!',
    FAILED_ADDING_CUSTOMER_RECORD: 'Failed to add customer record!',
    FAILED_ADDING_FABRIC_RECORD: 'Failed to add fabric record!',
    FAILED_ADDING_EMPLOYEE_RECORD: 'Failed to add employee record!',
    
}


export const error_messages = {

    BULK_UPLOAD: 'Error occurred during bulk upload!',
    
    INVALID_ORDER_BOOK_STATUS: "Invalid order book status!",

    //admin
    ADMIN_EXIST: 'Admin already exist!',
    ADMIN_NOT_EXIST: 'Admin does not exist!',

    ADD_ADMIN: 'The admin cannot be registered!',
    UPDATE_ADMIN: 'The admin cannot be updated!',
    DELETE_ADMIN: 'The admin cannot be deleted!',
    DELETE_ALL_ADMINS: 'There are no admins to delete!', 
    FETCH_ADMIN_BY_ID: 'The admin with the given ID was not found in the database!',


    //user
    USER_EXIST: 'User already exist!',
    USER_NOT_EXIST: 'User does not exist!',

    ADD_USER: 'The user cannot be registered!',
    UPDATE_USER: 'The user cannot be updated!',
    DELETE_USER: 'The user cannot be deleted!',
    DELETE_ALL_USERS: 'There are no users to delete!', 
    FETCH_USER_BY_ID: 'The user with the given ID was not found in the database!',
  

    //invoices
    INVOICE_EXIST: 'Invoice already exist!',
    INVOICE_NOT_EXIST: 'Invoice does not exist!',
    ADD_INVOICE: 'The invoice cannot be created!',
    UPDATE_INVOICE: 'The invoice cannot be updated!',
    DELETE_INVOICE: 'The invoice cannot be deleted!',
    DELETE_ALL_INVOICES: 'There are no invoices to delete!',
    FETCH_INVOICE_BY_ID: 'The invoice with the given ID was not found in the database!',
    FETCH_INVOICE_BY_CATEGORY: 'The invoice with the given Category ID was not found in the database!',
    FETCH_INVOICE_STOCK_COUNT:  'Invoice stock count cannot be fetched!',


    //product
    PRODUCT_EXIST: 'Product already exist!',
    PRODUCT_NOT_EXIST: 'Product does not exist!',
    ADD_PRODUCT: 'The product cannot be created!',
    UPDATE_PRODUCT: 'The product cannot be updated!',
    DELETE_PRODUCT: 'The product cannot be deleted!',
    DELETE_ALL_PRODUCTS: 'There are no products to delete!',
    FETCH_PRODUCT_BY_ID: 'The product with the given ID was not found in the database!',
    FETCH_PRODUCT_BY_CATEGORY: 'The product with the given Category ID was not found in the database!',
    FETCH_PRODUCT_STOCK_COUNT:  'Product stock count cannot be fetched!',
    INVALID_SIZES_ARRAY: 'Invalid sizes array!',


    //orderbook
    ORDER_BOOK_EXIST: 'Orde Book already exist!',
    ORDER_BOOK_NOT_EXIST: 'Orde Book does not exist!',
    ADD_ORDER: 'The order cannot be created!',
    UPDATE_ORDER: 'The order cannot be updated!',
    DELETE_ORDER: 'The order cannot be deleted!',
    DELETE_ALL_ORDER: 'There are no orders to delete!',

    FETCH_ORDER_BOOK_SALES: 'Order Book total sales cannot be fetched!',
    FETCH_ORDER_BY_ID: 'The order with the given ID was not found in the database!',
    ASSIGN_EMPLOYEE: 'The employee cannot be assigned!',

    //employee
    ADD_EMPLOYEE: 'The employee cannot be created!',
    UPDATE_EMPLOYEE: 'The employee cannot be updated!',
    DELETE_EMPLOYEE: 'The employee cannot be deleted!',
    DELETE_ALL_EMPLOYEES: 'There are no employees to delete!',
    EMPLOYEE_EXIST: 'Employee already exist!',
    FETCH_EMPLOYEE_BY_ID: 'The employee with the given ID was not found in the database!',

    //customer
    CUSTOMER_EXIST: 'Customer already exist!',
    CUSTOMER_NOT_EXIST: 'Customer does not exist!',
    ADD_CUSTOMER: 'The customer cannot be created!',
    UPDATE_CUSTOMER: 'The customer cannot be updated!',
    DELETE_CUSTOMER: 'The customer cannot be deleted!',
    DELETE_ALL_CUSTOMERS: 'There are no customers to delete!',
    FETCH_CUSTOMER_BY_ID: 'The customer with the given ID was not found in the database!',

    //fabric
    FABRIC_EXIST: 'Fabric already exist!',
    FABRIC_NOT_EXIST: 'Fabric does not exist!',
    ADD_FABRIC: 'The fabric cannot be created!',
    UPDATE_FABRIC: 'The fabric cannot be updated!',
    DELETE_FABRIC: 'The fabric cannot be deleted!',
    DELETE_ALL_FABRICS: 'There are no fabrics to delete!',
    FETCH_FABRIC_BY_ID: 'The fabric with the given ID was not found in the database!',

    //category
    CATEGORY_EXIST: 'Category already exist!',
    CATEGORY_NOT_EXIST: 'Category does not exist!',
    INVALID_CATEGORY_NAME: 'Invalid category name!',
    ADD_CATEGORY: 'The category cannot be created!',
    UPDATE_CATEGORY: 'The category cannot be updated!',
    DELETE_CATEGORY: 'The category cannot be deleted!',
    DELETE_ALL_CATEGORIES: 'There are no categories to delete!',
    FETCH_CATEGORY_BY_ID: 'The category with the given ID was not found in the database!',
}

export const success_messages = {
    BULK_UPLOAD: 'Bulk upload successfull!',

    //admin
    ADD_ADMIN: 'The admin registered successfully!',
    UPDATE_ADMIN: 'The admin updated successfully!',
    DELETE_ADMIN: 'The admin deleted successfully!',
    DELETE_ALL_ADMINS: 'All admins deleted successfully!', 
    FETCH_ADMINS: 'All admins fetched successfully!',
    FETCH_ADMIN_BY_ID: 'The admin with the given ID was found in the database!',
    FETCH_ADMIN_COUNT: 'Admin count fetched successfully!',
    ADMIN_LOGIN: 'Admin logged in successfully',



    //user
    ADD_USER: 'The user registered successfully!',
    UPDATE_USER: 'The user updated successfully!',
    DELETE_USER: 'The user deleted successfully!',
    DELETE_ALL_USERS: 'All users deleted successfully!', 
    FETCH_USERS: 'All users fetched successfully!',
    FETCH_USER_BY_ID: 'The user with the given ID was found in the database!',
    FETCH_USER_COUNT: 'User count fetched successfully!',
    USER_LOGIN: 'User logged in successfully',


    //invoices
    ADD_INVOICE: 'The invoice created successfully!',
    UPDATE_INVOICE: 'The invoice updated successfully!',
    DELETE_INVOICE: 'The invoice deleted successfully!',
    DELETE_ALL_INVOICES: 'All invoices deleted successfully!', 
    
    FETCH_INVOICES: 'All invoices fetched successfully!',
    FETCH_INVOICE_BY_ID: 'The invoice with the given ID was found in the database!',
    FETCH_INVOICE_BY_CATEGORY: 'The invoice with the given Category ID was found in the database!',
    FETCH_INVOICE_COUNT: 'Invoice count fetched successfully!',
    FETCH_INVOICE_STOCK_COUNT: 'Invoice stock count fetched successfully!',
    
    
    //products
    ADD_PRODUCT: 'The product created successfully!',
    UPDATE_PRODUCT: 'The product updated successfully!',
    DELETE_PRODUCT: 'The product deleted successfully!',
    DELETE_ALL_PRODUCTS: 'All products deleted successfully!', 
    
    FETCH_PRODUCTS: 'All products fetched successfully!',
    FETCH_PRODUCT_BY_ID: 'The product with the given ID was found in the database!',
    FETCH_PRODUCT_BY_CATEGORY: 'The product with the given Category ID was found in the database!',
    FETCH_PRODUCT_COUNT: 'Product count fetched successfully!',
    FETCH_PRODUCT_STOCK_COUNT: 'Product stock count fetched successfully!',

    //order
    ADD_ORDER: 'The order created successfully!',
    UPDATE_ORDER: 'The order updated successfully!',
    DELETE_ORDER: 'The order deleted successfully!',
    DELETE_ALL_ORDER: 'All orders deleted successfully!', 
    
    FETCH_ORDERS: 'All Orders fetched successfully!',

    FETCH_ORDER_BOOK_SALES: 'Order Book total sales fetched successfully!',
    FETCH_ORDER_BY_ID: 'The order with the given ID was found in the database!',
    FETCH_ORDER_BOOK_COUNT: 'Order Book count fetched successfully!',
    ASSIGN_EMPLOYEE: 'The employee assigned successfully!',


    //employee
    ADD_EMPLOYEE: 'The employee created successfully!',
    UPDATE_EMPLOYEE: 'The employee updated successfully!',
    DELETE_EMPLOYEE: 'The employee deleted successfully!',
    DELETE_ALL_EMPLOYEES: 'All employees deleted successfully!', 
    FETCH_EMPLOYEES: 'All Employees fetched successfully!',
    FETCH_EMPLOYEE_BY_ID: 'The employee with the given ID was found in the database!',
    FETCH_EMPLOYEE_COUNT: 'Employees count fetched successfully!',

    //customer
    ADD_CUSTOMER: 'The customer created successfully!',
    UPDATE_CUSTOMER: 'The customer updated successfully!',
    DELETE_CUSTOMER: 'The customer deleted successfully!',
    DELETE_ALL_CUSTOMERS: 'All customers deleted successfully!', 
    FETCH_CUSTOMERS: 'All Customer fetched successfully!',
    FETCH_CUSTOMER_BY_ID: 'The customer with the given ID was found in the database!',
    FETCH_CUSTOMER_COUNT: 'Customer count fetched successfully!',

    //fabrics
    ADD_FABRIC: 'The fabric created successfully!',
    UPDATE_FABRIC: 'The fabric updated successfully!',
    DELETE_FABRIC: 'The fabric deleted successfully!',
    DELETE_ALL_FABRICS: 'All fabrics deleted successfully!', 
    FETCH_FABRICS: 'All Fabric fetched successfully!',
    FETCH_FABRIC_BY_ID: 'The fabric with the given ID was found in the database!',
    FETCH_FABRIC_COUNT: 'Fabric count fetched successfully!',

    //category
    ADD_CATEGORY: 'The category created successfully!',
    UPDATE_CATEGORY: 'The category updated successfully!',
    DELETE_CATEGORY: 'The category deleted successfully!',
    DELETE_ALL_CATEGORIES: 'All categories deleted successfully!', 
    FETCH_CATEGORIES: 'All Categories fetched successfully!',
    FETCH_CATEGORY_BY_ID: 'The category with the given ID was found in the database!',
    FETCH_CATEGORIES_COUNT: 'Categories count fetched successfully!',

}

export const response_messages = {
    MISSING_PARAMETERS: 'Required parameters are missing!',
	INVALID_EMAIL: 'Invalid email or password.',
	ACCOUNT_ACTIVATED: 'Your account is successfully activated.',
	USER_EXISTS: 'User already exists.',
	RESET_MAIL_SENT: 'An email with password reset instructions has been sent to your email address.',
	PASSWORD_RESET: 'Your password has been reset successfully.',
	UNKNOWN_ERROR: 'Sorry something went wrong.',
	INVALID_SUPERADMIN: 'Invalid SuperAdmin.',
	NOT_AUTHORISED: 'You are not authorised to create SuperAdmin.',
	NOT_AUTHORISED_USER: 'You are not authorised to do this action.',
	USER_DOES_NOT_EXISTS: 'User does not exists.',
	USER_CREATED: 'User is successfully created.',
	USER_DELETED: 'User is successfully deleted.',
	INVALID_USER: 'Invalid User.',
	INVALID_ACCOUNDID: 'The account id is invalid.',
	NO_TOKEN: 'Access denied. No token provided.',
	INVALID_TOKEN: 'Access denied. Invalid Token.',
	LOGGED_OUT: 'User logged out successfully.',
	EMAIL_UPDATED: 'Email successfully updated.',
	USER_UPDATED: 'User info updated',
	NOT_ACTIVATED: 'Your account is not active.',
	PASSWORD_CHANGE: 'Your password has been changed successfully.',
	INCORRECT_PASSWORD: 'Entered password is incorrect, please try again.',
	SIGNED_LINK_ERROR: 'could not generate signed link.',
	SEND_ALL_DATA: 'please send all required information.',
	SEARCH_KEY_INVALID: 'search key invalid.',
	INVALID_STATUS: 'Please enter a valid status.',
}
