export const daysFilterFunction = {
    0: () => 3650,
    1: () => 1,
    2: () => 7,
    3: () => 30,
    4: () => {
        const now = new Date();
        const startOfYear = new Date(now.getFullYear(), 0);
        const diff = now - startOfYear;
        const oneDay = 1000 * 60 * 60 * 24;
        return Math.floor(diff / oneDay) + 1;
    },
};

export const defaultMaxDays = 3650;

export const genderArray = ['Male', 'Female']

export const customerSortFields = ['name', 'firstName', 'email', 'lastName', 'middleName', 'number', 'gender', 'address']

export const employeeSortFields = ['name', 'firstName', 'email', 'lastName', 'middleName', 'number', 'gender', 'address']

