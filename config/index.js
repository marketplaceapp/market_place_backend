
import dotenv from 'dotenv';
dotenv.config();

export const db_constants = {
    PORT: process.env.PORT,
    DB_NAME: process.env.DB_NAME,
    DB_USER: process.env.DB_USER,
    DB_PASS: process.env.DB_PASS,
    DB_CLUSTER_NAME: process.env.DB_CLUSTER_NAME,
    SECRET: "HTP",
}