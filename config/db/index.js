import mongoose from "mongoose"
import { db_constants } from '../index.js'

const { DB_USER, DB_PASS, DB_NAME, DB_CLUSTER_NAME } = db_constants;

const mongo_uri = `mongodb+srv://${DB_USER}:${DB_PASS}@${DB_CLUSTER_NAME}.oslkbv4.mongodb.net/?retryWrites=true&w=majority&appName=marketplace`

const connectDB = async () => {
    try {
        await mongoose.connect(mongo_uri)
        console.log("Connect to MongoDB successfully!")
    } catch (error) {
        console.log("Connect failed !! " + error.message )
    }
}

export default connectDB