import express from 'express';
import cors from 'cors';
import morgan from 'morgan';

import connectDB from './config/db/index.js'
import { routes } from './routes/index.js';

import { fileURLToPath } from 'url';
import path from 'path';



const app = express();

// Get directory name
const __dirname = path.dirname(fileURLToPath(import.meta.url));

// * Database connection
connectDB()

// * Cors
app.use(cors());
app.options("*", cors());

// * Middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(morgan("short"));


// Serve static files
//app.use('/public', express.static(path.resolve(__dirname, 'public')));

// app.use("/files", express.static("public"));
app.use('/public/uploads', express.static( __dirname + '/public/uploads'));

// * Error Handler
// app.use(authJwt());
// app.use(errorHandler);

// * Api routes
app.use("/api/v1", routes);

app.get("/get_api", (req, res) => {
    console.log("hello");
    res.send("hello");
});

app.use("*", (req, res) => {
    res.status(400).send({ success: false, message: 'Route not found' });
});

let PORT = process.env.PORT || 3000;

app.listen(PORT, () => console.log(`Server is running on PORT ${PORT}`));
