import express from "express";
import {
    addProduct, 
    deleteAllProduct, 
    deleteProduct, 
    getAllProduct,
    getOwnersProductAds,
    getProductAds,
    getProductById,
    updateProduct,
} from "../../controllers/products/index.js";

import { uploadOptions } from "../../utils/utils.js";

const router = express.Router();

// ROUTES * /api/v1/products/
 
router.get("/", getAllProduct);
router.get("/get_ads", getProductAds);
router.get("/get_owners_ads", getOwnersProductAds);

router.get("/:id", getProductById);
router.delete("/:id", deleteProduct);

router.put("/", updateProduct);



router.post("/", uploadOptions.single('image'), addProduct);
router.post("/delete_products", deleteAllProduct);

export const products = router;
