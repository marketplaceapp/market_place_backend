import express from "express";
import {
    addUser, 
    deleteAllUser, 
    getAllUser,
    getUserById,
} from "../../controllers/users/index.js";
import { uploadOptions } from "../../utils/utils.js";

const router = express.Router();

// ROUTES * /api/v1/users/
 
router.get("/", getAllUser);
router.get("/:id", getUserById);
router.post("/", uploadOptions.single('image'), addUser);
router.post("/delete_users", deleteAllUser);

export const users = router;
