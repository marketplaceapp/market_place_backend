import express from "express";
import authMiddleware from "../middleware/authMiddleware.js";

import { products } from "./products/index.js";
import { users } from "./users/index.js";
import { auth } from "./auth/index.js";

const router = express.Router();

// PRODUCTS Routes * /api/products/*
// USERS Routes * /api/users/*

router.use("/products", authMiddleware, products);
router.use("/users", authMiddleware, users);

router.use("/auth/user", auth);

export const routes = router;
