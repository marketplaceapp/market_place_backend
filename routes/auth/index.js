import express from "express";
import { loginUser, registerUser } from "../../controllers/auth/index.js";

const router = express.Router();

// ROUTES * /api/v1/auth/user/
router.post("/register", registerUser);
router.post("/login", loginUser);

export const auth = router;
