import { FailureResponse } from "../models/response/globalResponse.js";
import { error_messages, global_messages } from "../utils/constants.js";

const asyncMiddleware = (handler) => async (req, res, next) => {
    try {
        await handler(req, res);
    } catch (error) {
        console.log("CATCH ERROR");
        console.log("Error :: ", error);
        const statusCode = error?.statusCode || 500;
        const message = error?.message || global_messages.INTERNAL_SERVER_ERROR; 
        res.status(statusCode).json(new FailureResponse(message))

        next(error)
    }
};

export default asyncMiddleware;