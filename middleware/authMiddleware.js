import jwt from 'jsonwebtoken';
import { SuccessResponse, FailureResponse, } from '../models/response/globalResponse.js';
import { error_messages, global_messages, success_messages } from '../utils/constants.js';

import { User } from '../models/users/index.js';


const JWT_SECRET = process.env.secret;

const authMiddleware = async (req, res, next) => {
    var token = req.header('Authorization');
    //console.log("token> > ",token);
    
    if(token && token.startsWith('Bearer')){
        token = token.split(' ')[1];
    }

    if (!token) {
        return res.status(401).json(new FailureResponse(global_messages.NO_TOKEN));
    }

    try {
        const decoded = jwt.verify(token, JWT_SECRET);
        //console.log("decoded > > ",decoded);

        req.user = await User.findById(decoded.userId)
        req.authToken = token

        console.log("LoggedIn USER> > ",req.user.id);

        next();
    } catch (err) {
        return res.status(401).json(new FailureResponse(global_messages.INVALID_TOKEN));
    }
};

export default authMiddleware;
