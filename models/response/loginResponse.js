export class LoginResponse {
	constructor(json) {
		if (json && typeof json === 'object') {
			this.profile = json.profile;
			this.isSuperAdmin = json.isSuperAdmin;
			this.permissions = json.permissions ?? [];
			this.authToken = json.authToken;
			this.pushNotificationEnabled = json.pushNotificationEnabled;
		}
	}
}

export class Profile {
	constructor(json) {
		if (json && typeof json === 'object') {
			this.email = json.email;
			this.firstName = json.firstName;
			this.middleName = json.middleName;
			this.lastName = json.lastName;
			this.fullName = json.fullName;
			this.dob = json.dob;
			this.gender = json.gender;
		}
	}
}
