export class SuccessResponse {
    constructor(value, data, token, total_count) {
        this.success = true;
        this.message = value;
        this.data = data;
        this.token = token;
        this.total_count = total_count;
    }
}
  
export class FailureResponse {
    constructor(value, data) {
        this.success = false;
        this.message = value || 'Error Message';
        this.data = data;
    }
}
