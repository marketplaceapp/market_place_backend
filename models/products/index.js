import mongoose from 'mongoose';

const productSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: true
    },
    image: {
        type: String,
        default: '',
        require: true
    },
    // images: [{
    //     type: String
    // }],
    price : {
        type: Number,
        default: 0
    },
    category: {
        type: String,
        required: true
    },
    sub_category: {
        type: String,
        required: true
    },
    discount_amount: {
        type: Number,
        default: 0,
    },
    dateCreated: {
        type: Date,
        default: Date.now,
    },
    status: {
        type: String,
        default: 'active',
    },
    adsOwner: {
        type: Object,
        default: null,
    }
},
{
    timestamps: true
}
)

productSchema.virtual('id').get(function () {
    return this._id.toHexString();
});

productSchema.set('toJSON', {
    virtuals: true,
});



export const Product = mongoose.model('Product', productSchema);
